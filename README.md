<p align="center">
  <img src="https://storage.googleapis.com/gweb-uniblog-publish-prod/images/In-lineImage_1500x850.max-1000x1000.jpg" /> 
</p>

# Android Open Source Project #

### Getting started:
To get started, you'll need to get familiar with [Repo](https://source.android.com/source/using-repo.html) and Version Control with [Git](https://source.android.com/source/version-control.html). 

```bash
# Initialize local repository
repo init -u https://gitlab.com/PixelBoot/android_manifest -b android-10.0.0

# Sync
repo sync 
```

### Build ###

```bash
# Set up environment
$ . build/envsetup.sh

# Choose a target
$ lunch aosp_$device-userdebug

# Build the code
$ make otapackage 
```
